package com.random;

public class Main {



    public static void main(String[] args) {

        RandomService service = new RandomService();

        // Вывести на консоль случайное число.
        System.out.println("случайное число - "+ service.randomizeNumber(0,100));

        // Вывести на консоль 10 случайных чисел.
        service.secondTask();

        // Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
        service.thirdTask();

        // Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50
        service.fourthTask();

        // Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
        service.fifeTask();

        //Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.
        service.sixthTask();















    }
}
